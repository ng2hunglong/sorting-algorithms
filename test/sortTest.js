const assert = require('assert');
const mergeSort = require('../sortFunctions/mergeSort');
const quickSort = require('../sortFunctions/quickSort');
const heapSort = require('../sortFunctions/heapSort');
const radixSort = require('../sortFunctions/radixSort');
const timSort = require('../sortFunctions/timSort');

let samples = [
	[-1],
	[4, 2],
	[-1, -3, -5, 2, 2, 5],
	[0,0,-1,-112,-121,112,121,312,9],
	[-10,-22,-5,111,0,22,15,134,-444],
];
let results = [
	[-1],
	[2, 4],
	[-5, -3, -1, 2, 2, 5],
	[-121,-112,-1,0,0,9,112,121,312],
	[-444,-22,-10,-5,0,15,22,111,134],
];

let sortObj = {
	"mergeSort": mergeSort,
	"quickSort": quickSort,
	"heapSort": heapSort,
	"radixSort": radixSort,
	"timSort": timSort,
};

for (let [funcName, func] of Object.entries(sortObj)) {
	describe(funcName, () => {
		for (let i in samples) {
			it(`Array [${samples[i].join()}] return [${results[i].join()}]`, function() {
				assert.deepStrictEqual(func[funcName](samples[i]), results[i]);
			})
		}
	});	
}
