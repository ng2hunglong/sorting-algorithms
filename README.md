
**Requisite**

1. npm.
2. node.

---

## Installation

1. Clone this project.
2. in terminal, install depenencies with this command: **npm install**

---

## Running test

* Sorting functions are stored in sortFunctions/ folder.

* Testing with mocha with this file: test/sortTest.js.

* To run test: run this command in terminal: **npm run test**