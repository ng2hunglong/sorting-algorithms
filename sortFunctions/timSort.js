module.exports = {
  timSort(arr) {
    let set = new Set([...arr]);
    if (arr.length < 2 || set.size < 2) {
      return arr;
    }
    if (arr.length <= 4) {
      return this.insertSort(arr);
    }
    let leftArr = arr.slice(0, Math.floor(arr.length/2));
    let rightArr = arr.slice(Math.floor(arr.length/2), arr.length);
    return this.mergeSortedArr(this.timSort(leftArr), this.timSort(rightArr));
  },
  insertSort(arr) {
    for (let i = 1; i < arr.length; i++) {
      for (let j = i - 1; j >= 0; j--) {
        if (arr[j+1] < arr[j]) {
          arr[j+1] += arr[j];
          arr[j] = arr[j+1] - arr[j];
          arr[j+1] = arr[j+1] - arr[j];
        } else {
          break;
        }
      }
    }
    return arr;
  },
  mergeSortedArr(arr1, arr2) {
    let result = [];
    while(arr1.length || arr2.length) {
      if (!arr1.length || arr2[0] < arr1[0]) {
        result.push(arr2[0]);
        arr2.shift();
      } else {
        result.push(arr1[0]);
        arr1.shift();
      }
    }
    return result;
  }
}