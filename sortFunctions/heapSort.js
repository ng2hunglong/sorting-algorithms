module.exports = {
  heapSort(arr) {
    if (arr.length < 2) return arr;
    let arrLength = arr.length;
    while(arrLength > 1) {
      this.createMaxHeap(arr, arrLength);
      this.swapNode(arr, 0, arrLength-1);
      arrLength--;
    }
    return arr;
  },
  createMaxHeap(arr, arrLength) {
    let leftChildIndex;
    let rightChildIndex;
    let i = arrLength -1;
    let parentNodeIndex;
    while (i > 0) {
      if (i % 2 == 0) {
        rightChildIndex = i;
        leftChildIndex = i - 1;
        parentNodeIndex = i / 2 - 1;
        i = i - 2;
      } else {
        leftChildIndex = i;
        rightChildIndex = null;
        parentNodeIndex = (i - 1) / 2;
        i = i - 1;
      }
      let indexOfMaxChild = leftChildIndex;
      if (rightChildIndex && arr[rightChildIndex] > arr[leftChildIndex]) {
        indexOfMaxChild = rightChildIndex;
      }
      if (arr[indexOfMaxChild] > arr[parentNodeIndex]) {
        this.swapNode(arr, parentNodeIndex, indexOfMaxChild);
      }
    }
  },
  swapNode(arr, i1, i2) {
    arr[i1] += arr[i2];
    arr[i2] = arr[i1] - arr[i2];
    arr[i1] = arr[i1] - arr[i2];
  }
}