module.exports = {
  quickSort(arr) {
    let set = new Set([...arr]);
    if (arr.length < 2 || set.size < 2) {
      return arr;
    }
    let pivot = arr[0];
    let leftArr = [];
    let rightArr = [];
    for (let i= 1; i < arr.length; i++) {
      if (arr[i] < pivot) {
        leftArr.push(arr[i]);
      } else {
        rightArr.push(arr[i]);
      }
    }
    return [...this.quickSort(leftArr), pivot, ...this.quickSort(rightArr)];
  }
}