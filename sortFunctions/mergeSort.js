module.exports = {
	mergeSort(arr) {
		if (arr.length < 2) {
			return arr;
		}

		let leftArr = [];
		for (let i = 0; i < Math.floor(arr.length/2); i++) {
			leftArr.push(arr[i]);
		}
		
		let rightArr = [];
		for (let i = Math.floor(arr.length/2); i < arr.length; i++) {
			rightArr.push(arr[i]);
		}
		return this.mergeArr(this.mergeSort(leftArr), this.mergeSort(rightArr));
	},

	mergeArr(leftArr, rightArr) {
		let result = [];
		while(leftArr.length > 0 || rightArr.length > 0) {
			if (!rightArr.length || leftArr[0] <= rightArr[0]) {
				result.push(leftArr[0]);
				leftArr.shift();
			} else {
				result.push(rightArr[0]);
				rightArr.shift();
			}
		}
		return result;
	},
}