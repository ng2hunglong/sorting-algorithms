module.exports = {
  findMaxDigitLength(arr) {
    let maxDigitLength = 0;
    for (let val of arr) {
      if (maxDigitLength < val.toString().replace('-', '').length) {
        maxDigitLength = val.toString().replace('-', '').length;
      }
    }
    return maxDigitLength;
  },

  radixSort(arr) {
    if (arr.length < 2) return arr;
    let digitPosition = 0;
    while(digitPosition < this.findMaxDigitLength(arr)) {
      this.sortByDigit(arr, digitPosition);
      digitPosition++;
    }
    return arr;
  },

  sortByDigit(arr, position) {
    let positiveBucket = new Map();
    for (let i = 0; i < 10; i++) {
      positiveBucket.set(i.toString(), []);
    }
    let negativeBucket = new Map();
    for (let i = -9; i < 1; i++) {
      negativeBucket.set(i.toString(), []);
    }
    while (arr.length > 0) {
      let currentDigit = Math.floor(Math.abs(arr[0])/10**position) % 10;
      if (arr[0] < 0) {
        if (currentDigit !== 0) currentDigit *= -1;
        negativeBucket.set(currentDigit.toString(), [...negativeBucket.get(currentDigit.toString()), arr[0]]);
      } else {
        positiveBucket.set(currentDigit.toString(), [...positiveBucket.get(currentDigit.toString()), arr[0]]);
      }
      arr.shift();
    }
    for (let digitArrGroup of negativeBucket.values()) {
      if (digitArrGroup.length == 0) continue;
      arr.push(...(digitArrGroup));
    }
    for (let digitArrGroup of positiveBucket.values()) {
      if (digitArrGroup.length == 0) continue;
      arr.push(...(digitArrGroup));
    }
  }
}