var options = {
	enableHighAccuracy: true,
	timeout: 5000,
	maximumAge: 0,
}
function success(pos) {
	var crd = pos.coords;
	console.log(`Your current position is:
Latitude: ${crd.latitude}
Longitutde: ${crd.longitude}
More or less: ${crd.accuracy} metter.`);
}
function error(err) {
	console.warn(`ERROR(${err.code}: ${err.message}`);
}
navigator.geolocation.getCurrentPosition(success, error, options);
